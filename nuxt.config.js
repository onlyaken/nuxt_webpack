const setting = require('./src/setting.config.js')
const config = {
  mode: setting.mode || 'universal', //universal, spa
  head: setting.head,
  loading: setting.loading || {},
  css: setting.css || [],
  plugins: setting.plugins || [],
  buildModules: setting.buildModules || [],
  modules: setting.modules || [],
  axios: setting.axios || {},
  build: {
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/,
          options:{
            fix: true,
          }
        })
      }
    },
    postcss: {
      // 添加插件名称作为键，参数作为值
      // 使用npm或yarn安装它们
      // plugins: {
      //   // 通过传递 false 来禁用插件
      //   'postcss-url': false,
      //   'postcss-nested': {},
      //   'postcss-responsive-type': {},
      //   'postcss-hexrgba': {}
      // },
      preset: {
        // 更改postcss-preset-env 设置
        autoprefixer: {
          grid: true
        }
      }
    }
  },
  server: setting.server,
  srcDir:'src/',
}

for (const key in setting) {
  if (!config.hasOwnProperty(key)) {
    config[key] = setting[key]
  }
}

module.exports = config
