# nuxt_freamwork

> [Nuxt.js | 官網](https://zh.nuxtjs.org/guide)
> [Bitbucket | nuxt_webpack](https://bitbucket.org/onlyaken/nuxt_webpack/src/master/) 
>

第一次安裝
---

#### 1. 先下載 webpack
``` bash
## mynuxt 是你的專案名稱
git clone https://bitbucket.org/onlyaken/nuxt_webpack.git mynuxt
```

#### 2. 製作自己的 git 專案(如果沒有 git 那就先刪除掉本機的 .git)
``` bash
cd munuxt

## 刪除 .git
sudo rm -rf .git

## 初始化 git
git init 
git remote add origin git@bitbucket.org:xxxxxxx/xxxxxxx.git

## 第一次使用 git 記得先建立個人資訊
git config --global user.name "Aken"
git config --global user.email "xxxxx@example.com"
```

#### 3. 回到你的專案目錄，安裝 npm 套件
``` bash
npm i
```
#### 4. 現在執行 `npm run dev` 會看到 `http://location:3001`，打開之後就可以看到網頁了 

---

## Build Setup

``` bash
# install dependencies
$ npm i

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

---

## 版本更新

### 2020-01-20
> v 1.0.1
> 

套件更新

 1. nuxt: 2.9.0 => 2.11.0
 2. @vue/test-utils: 1.0.0-beta.27 => 1.0.0-beta.30
 3. handlebars: 4.5.1 => 4.7.2

套件移除

1. postcss-url
2. postcss-nested
3. postcss-responsive-type
4. postcss-hexrgba