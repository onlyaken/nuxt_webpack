const setting = {
  mode: 'universal', //universal, spa
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href:'https://fw.akensite.ml/cdn/v1.0.0/igold.min.css'},
    ],
    script: [
      {src: 'https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js', async:true, defer: true},
      {src: 'https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.auto.min.js', async:true, defer: true},
      {src: 'https://cdn.jsdelivr.net/npm/es6-promise@4/dist/es6-promise.min.js', async:true, defer: true},
    ],
    htmlAttrs: {
      lang: 'zh',
    },
  },
  loading: { color: '#fff' },
  css: [
    '@/assets/scss/extends.scss',
  ],
  plugins: [
    '@/plugins/my-ui.js'
  ],
  buildModules: [
    '@nuxtjs/eslint-module'
  ],
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],
  axios: {
  },
  server: {
    port: 3001, // default: 3000
    host: 'localhost', // default: localhost,
    timing: false
  },
  watch: [
    '@/store/*.js', 
    '@/setting.config.js', 
    // '@/components/*', 
    '@/components/**/*'
  ],
}

module.exports = setting